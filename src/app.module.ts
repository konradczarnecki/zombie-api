import { Module } from '@nestjs/common';
import { ZombieModule } from './modules/zombie/zombie.module';
import { ItemModule } from './modules/item/item.module';
import { ConfigModule } from 'nestjs-config';
import { ExchangeModule } from './modules/exchange/exchange.module';
import * as path from 'path';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    ConfigModule.load(path.resolve(__dirname, 'config/**/*.{ts,js}')),
    MongooseModule.forRootAsync({
      useFactory: () => ({
        uri: process.env.DB_URI
      })
    }),
    ZombieModule,
    ItemModule,
    ExchangeModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
