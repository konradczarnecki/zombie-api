import { HttpModule, Module } from '@nestjs/common';
import { ZombieService } from './zombie.service';
import { ZombieController } from './zombie.controller';
import { ItemModule } from '../item/item.module';
import { ExchangeModule } from '../exchange/exchange.module';
import { Zombie, ZombieModel } from './model/zombie.model';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Zombie', schema: ZombieModel.schema }]),
    ItemModule,
    ExchangeModule,
    HttpModule
  ],
  providers: [ZombieService],
  controllers: [ZombieController]
})
export class ZombieModule {}
