import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from 'typegoose';
import { Zombie } from './model/zombie.model';
import { UpdateZombieDTO } from './model/update-zombie.dto';
import { ItemService } from '../item/item.service';
import { Item } from '../item/model/item.model';
import { ExchangeService } from '../exchange/exchange.service';
import { GetZombieDTO } from './model/get-zombie.dto';
import { CreateZombieDTO } from './model/create-zombie.dto';
import { GetZombieDetailsDTO } from './model/get-zombie-details.dto';

@Injectable()
export class ZombieService {

  constructor(
    @InjectModel('Zombie') protected readonly model: ModelType<Zombie>,
    private itemService: ItemService,
    private exchangeService: ExchangeService
  ) {}

  async findByName(name: string) {

    await this.itemService.checkForUpdate();

    const zombies = await this.model.find({ name: { $regex: new RegExp(name, 'i')}}).populate('items');

    return zombies.map(zombie => zombie.toJSON());
  }

  async findById(id: string): Promise<GetZombieDetailsDTO> {

    await this.itemService.checkForUpdate();

    const zombie = await this.model.findById(id).populate('items');
    const totalPln = (zombie.items as Item[]).reduce((prev, curr) => prev + curr.price, 0);

    return {
      ...zombie.toJSON(),
      itemsTotal: {
        pln: totalPln,
        usd: this.exchangeService.plnToUsd(totalPln),
        eur: this.exchangeService.plnToEur(totalPln)
      }
    };
  }

  async create(data: CreateZombieDTO) {
    return (await this.model.create(data)).toJSON();
  }

  async update(id: string, data: UpdateZombieDTO) {

    if (data.addItems) {
      const itemsToAdd = await this.itemService.getItemsByIds(data.addItems);
      await this.model.findByIdAndUpdate(id, { $push: { items: { $each: itemsToAdd.map(item => item._id) }}});
    }

    if (data.removeItems) {
      const itemsToRemove = await this.itemService.getItemsByIds(data.removeItems);
      await this.model.findByIdAndUpdate(id, { $pullAll: { items: itemsToRemove.map(item => item._id) }});
    }

    if (data.name) {
      await this.model.findByIdAndUpdate(id, { name: data.name });
    }

    return (await this.model.findById(id)).toJSON();
  }

  async delete(id: string) {
    return await this.model.deleteOne({ _id: id });
  }
}
