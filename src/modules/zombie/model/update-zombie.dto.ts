import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateZombieDTO {

  @ApiModelProperty()
  name?: string;

  @ApiModelProperty({
    description: 'Array of item ids to add to zombie',
    isArray: true,
    type: Number
  })
  addItems?: number[];

  @ApiModelProperty({
    description: 'Array of item ids to remove from zombie',
    isArray: true,
    type: Number
  })
  removeItems?: number[];
}
