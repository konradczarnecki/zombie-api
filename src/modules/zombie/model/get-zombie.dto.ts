import { ApiModelProperty } from '@nestjs/swagger';

export class GetZombieDTO {

  @ApiModelProperty()
  name: string;

  @ApiModelProperty()
  items: string[];

  constructor(data: Partial<GetZombieDTO>) {
    Object.assign(this, data);
  }
}
