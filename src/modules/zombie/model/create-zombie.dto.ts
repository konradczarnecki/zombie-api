import { ApiModelProperty } from '@nestjs/swagger';

export class CreateZombieDTO {

  @ApiModelProperty()
  name: string;
}
