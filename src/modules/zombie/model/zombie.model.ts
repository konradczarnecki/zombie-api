import { arrayProp, prop, Ref, Typegoose } from 'typegoose';
import { Item } from '../../item/model/item.model';

export class Zombie extends Typegoose {

  @prop()
  name: string;

  @arrayProp({ itemsRef: Item })
  items: Ref<Item>[];
}

export const ZombieModel = new Zombie().getModelForClass(Zombie);
