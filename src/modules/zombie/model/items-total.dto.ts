import { ApiModelProperty } from '@nestjs/swagger';

export class ItemsTotalDTO {

  @ApiModelProperty()
  pln: number;

  @ApiModelProperty()
  usd: number;

  @ApiModelProperty()
  eur: number;
}
