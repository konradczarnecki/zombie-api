import { GetZombieDTO } from './get-zombie.dto';
import { ApiModelProperty } from '@nestjs/swagger';
import { ItemDTO } from '../../item/model/item.dto';
import { ItemsTotalDTO } from './items-total.dto';

export class GetZombieDetailsDTO {

  @ApiModelProperty()
  name: string;

  @ApiModelProperty({
    type: ItemDTO,
    isArray: true
  })
  items: ItemDTO[];

  @ApiModelProperty({
    type: ItemsTotalDTO
  })
  itemsTotal: ItemsTotalDTO;

  constructor(data: Partial<GetZombieDetailsDTO>) {
    Object.assign(this, data);
  }
}
