import { Body, Controller, Delete, Get, Param, Post, Put, Query } from '@nestjs/common';
import { ZombieService } from './zombie.service';
import { GetZombieDetailsDTO } from './model/get-zombie-details.dto';
import { GetZombieDTO } from './model/get-zombie.dto';
import { CreateZombieDTO } from './model/create-zombie.dto';
import { UpdateZombieDTO } from './model/update-zombie.dto';
import { ApiImplicitBody, ApiImplicitParam, ApiImplicitQuery, ApiOkResponse } from '@nestjs/swagger';

@Controller('zombie')
export class ZombieController {

  constructor(
    private zombieService: ZombieService
  ) {}

  @Get(':id')
  @ApiImplicitParam({
    name: 'id',
    description: 'Mongo id of zombie details to get'
  })
  @ApiOkResponse({
    type: GetZombieDetailsDTO
  })
  async getOne(@Param('id') id: string) {
    const zombie = await this.zombieService.findById(id);
    return new GetZombieDetailsDTO(zombie);
  }

  @Get()
  @ApiImplicitQuery({
    name: 'name',
    description: 'Name of the zombie to search by'
  })
  @ApiOkResponse({
    type: GetZombieDTO,
    isArray: true
  })
  async getAll(@Query('name') name: string) {
    const zombies = await this.zombieService.findByName(name);
    return zombies.map(zombie => new GetZombieDTO(zombie));
  }

  @Post()
  @ApiOkResponse({
    type: GetZombieDTO
  })
  async create(@Body() data: CreateZombieDTO) {
    const zombie = await this.zombieService.create(data);
    return new GetZombieDTO(zombie);
  }

  @Put(':id')
  @ApiImplicitParam({
    name: 'id',
    description: 'id of the zombie to update'
  })
  @ApiOkResponse({
    type: GetZombieDTO
  })
  async update(@Param('id') id: string, @Body() data: UpdateZombieDTO) {
    const zombie = await this.zombieService.update(id, data);
    return new GetZombieDTO(zombie);
  }

  @Delete(':id')
  @ApiImplicitParam({
    name: 'id',
    description: 'id of the zombie to delete'
  })
  @ApiOkResponse({
    type: { status: 'ok' }
  })
  async delete(@Param('id') id: string) {
    await this.zombieService.delete(id);
    return { status: 'ok' };
  }
}
