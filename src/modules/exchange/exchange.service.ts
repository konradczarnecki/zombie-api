import { Injectable } from '@nestjs/common';

@Injectable()
export class ExchangeService {

  plnToUsd(plnValue: number) {
    return plnValue * 4.2;
  }

  plnToEur(plnValue: number) {
    return plnValue * 3.7;
  }
}
