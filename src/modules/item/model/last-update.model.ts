import { prop, Typegoose } from 'typegoose';

export class LastUpdate extends Typegoose {

  @prop()
  timestamp: number;
}

export const LastUpdateModel = new LastUpdate().getModelForClass(LastUpdate);
