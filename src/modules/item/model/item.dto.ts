import { ApiModelProperty } from '@nestjs/swagger';

export class ItemDTO {

  @ApiModelProperty()
  id: number;

  @ApiModelProperty()
  name: string;

  @ApiModelProperty()
  price: number;
}
