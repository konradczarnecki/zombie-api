import { Item } from './item.model';

export interface UpdateData {
  timestamp: number;
  items: Item[];
}
