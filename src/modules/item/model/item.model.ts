import { ModelType, prop, Typegoose } from 'typegoose';

export class Item extends Typegoose {

  @prop()
  id: number;

  @prop()
  name: string;

  @prop()
  price: number;

  @prop()
  updated: number;
}

export const ItemModel = new Item().getModelForClass(Item);
