import { HttpModule, Module } from '@nestjs/common';
import { ItemService } from './item.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Item, ItemModel } from './model/item.model';
import { LastUpdate, LastUpdateModel } from './model/last-update.model';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Item', schema: ItemModel.schema },
      { name: 'LastUpdate', schema: LastUpdateModel.schema }
    ]),
    HttpModule,
  ],
  providers: [ItemService],
  exports: [ItemService],
})
export class ItemModule {
}
