import { HttpService, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from 'typegoose';
import { ConfigService } from 'nestjs-config';
import { UpdateData } from './model/update-data';
import { Item } from './model/item.model';
import { LastUpdate } from './model/last-update.model';

@Injectable()
export class ItemService {

  constructor(
    @InjectModel('Item') protected readonly model: ModelType<Item>,
    @InjectModel('LastUpdate') protected readonly lastUpdateModel: ModelType<LastUpdate>,
    private http: HttpService,
    private config: ConfigService
  ) {}

  async getItemsByIds(itemIds: number[] = []) {

    await this.checkForUpdate();

    return await this.model.find({ id: { $in: itemIds } });
  }

  async checkForUpdate() {

    const lastUpdate = await this.lastUpdateModel.findOne();

    if (!lastUpdate) {
      await this.lastUpdateModel.create({});
      return this.updateItems();

    } else if ((new Date().getTime() - lastUpdate.timestamp) < 1000 * 60 * 60 * 24) {
      return this.updateItems();
    }
  }

  async updateItems() {

    const updateResponse = await this.http.get(this.config.get('app.itemsMarketUrl')).toPromise();
    const data: UpdateData = updateResponse.data;

    await this.lastUpdateModel.update({}, { timestamp: data.timestamp });
    await Promise.all(data.items.map(itemData => this.model.update({ id: itemData.id }, itemData, { upsert: true })));
  }
}
