export default {
  itemsMarketUrl: 'https://zombie-items-api.herokuapp.com/api/items',
  exchangeRatesUrl: 'http://api.nbp.pl/api/exchangerates/tables/C/today'
};
